# About Application

This application is used to send SMS using MojaGate SMS gateway https://docs.mojasms.dev/#introduction

# Getting Started

The following steps is how to set up your project.

## Installation & Running The Application

1. Clone the project to your machine. 

```
$ git clone https://gitlab.com/kibetbrian/mojagate-sms.git

```  

2. Install composer packages

```
$ composer install OR 
$ composer update

```  
3. Create .env file
4. Create your database
5. Migrate your database 

```
$ php artisan migrate

``` 

6. Run a worker

```
$ php artisan queue:work

``` 

7. Start laravel local server 

```
$ php artisan serve

``` 

## Contact

Name: Brian Kiptanui.
Email: kimutaibrian9@gmail.com

## Acknowledgments

Mojagate developers
