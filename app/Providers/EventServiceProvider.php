<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use App\Listeners\SendMessageListener;
use Illuminate\Auth\Events\Registered;
use App\Events\NewMessageReceivedEvent;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        NewMessageReceivedEvent::class => [
            SendMessageListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
