<?php

namespace App\Listeners;

use App\Http\Traits\SendSmsTrait;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendMessageListener implements ShouldQueue
{
    use SendSmsTrait;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $phone      = $event->phone;
        $message    = $event->message;

        $this->sendSms($message, $phone);

    }
}
