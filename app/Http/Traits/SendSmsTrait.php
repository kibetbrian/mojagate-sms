<?php

namespace App\Http\Traits;

use App\Models\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

trait SendSmsTrait
{
    public function mojagate_email()
    {
        Cache::put('email', env('MOJAGATE_EMAIL'));
    }

    public function mojagate_password()
    {
        Cache::put('password', env('MOJAGATE_PASSWORD'));
    }

    public function authToken()
    {
        $email      = Cache::get('email');
        $password   = Cache::get('password');

       $client = new \GuzzleHttp\Client();
       $response = $client->post(
           'https://api.mojasms.dev/login',
           [
               'headers' => [
                   'Accept' => 'application/json',
               ],
               'json' => [
                   'email'      => $email,
                   'password'   => $password,
               ],
           ]
       );
       $body = $response->getBody();
       $api = json_decode((string) $body);

       return $api->data->token;

    }
    public function uniqueID()
    {
        $uniqueNumber = sprintf("%06d", mt_rand(1, 999999));

        return $uniqueNumber;
    }

    public function sendSms($message, $phone)
    {

        $messageID  = $this->uniqueID();
        $token      = $this->authToken();
        $from       = 'MojaSMS';

        $client = new \GuzzleHttp\Client();
        $response = $client->post(
            'https://api.mojasms.dev/sendsms',
            [
                'headers' => [
                    'Authorization' => "Bearer {$token}",
                    'Accept' => 'application/json',
                ],
                'json' => [
                    'phone'         => $phone,
                    'message'       => $message,
                    'from'          => $from,
                    'message_id'    => $messageID,
                    'webhook_url'   => 'https://mojagate.com/sms-webhook',
                ],
            ]
        );
        $body = $response->getBody();
        $callBack = json_decode((string) $body);

        $all = $callBack->data->recipients;

        $time_sent  = $all[0]->created_at;

        $time = date("Y-m-d H:i:s", strtotime($time_sent));

        $message = Message::latest('id')->first();

        DB::table('messages')
              ->where('id', $message->id)
              ->update([
                  'status'      => 1,
                  'time_sent'   => $time
                ]);
    }
}
