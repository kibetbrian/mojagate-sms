<?php

namespace App\Http\Controllers;

use App\Events\NewMessageReceivedEvent;
use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{

    public function index()
    {
          $records = Message::all();

        return view('admin1.dashboard',compact('records'));
    }


    public function receiveMessage(request $request)
    {

        $request->validate([
            'phone'    => 'required|numeric',
            'message'   => 'required',
        ]);

        $phone      = $request->phone;
        $message    = $request->message;

        Message::create([
            'phone'      => $phone,
            'message'    => $message,
            'time_sent'  => date("Y-m-d H:i:s"),
        ]);

        NewMessageReceivedEvent::dispatch($phone,$message);

        return back();
    }

}
