  <!-- .app-aside -->
  <aside class="app-aside app-aside-expand-md app-aside-light">
      <!-- .aside-content -->
      <div class="aside-content">
          <!-- .aside-header -->
          <header class="aside-header d-block d-md-none">
              <!-- .btn-account -->
              <button class="btn-account" type="button" data-toggle="collapse" data-target="#dropdown-aside"><span
                      class="user-avatar user-avatar-lg"><img  src="{{asset('assets/profile.jpg')}}" alt=""></span>
                  {{--  <span class="account-icon"><span class="fa fa-caret-down fa-lg"></span></span> <span
                      class="account-summary"><span class="account-name">{{$res->firstName}} {{$res->lastName}}</span> <span  --}}
                          class="account-description"></span></span></button> <!-- /.btn-account -->
              <!-- .dropdown-aside -->
              <div id="dropdown-aside" class="dropdown-aside collapse">
                  <!-- dropdown-items -->
                  <div class="pb-3">
                     
                      <div class="dropdown-divider"></div>
                  </div><!-- /dropdown-items -->
              </div><!-- /.dropdown-aside -->
          </header><!-- /.aside-header -->
          <!-- .aside-menu -->
          <div class="aside-menu overflow-hidden">
              <!-- .stacked-menu -->
              <nav id="stacked-menu" class="stacked-menu">
                  <!-- .menu -->
                  <ul class="menu">
                      <!-- .menu-item -->
                      <li class="menu-item has-active">
                          <a href="#" class="menu-link"><span class="menu-icon fas fa-envelope-square"></span>
                              <span class="menu-text">Message</span></a>
                      </li><!-- /.menu-item -->

                  <!-- .menu-item -->

                  </ul><!-- /.menu -->
              </nav><!-- /.stacked-menu -->
          </div><!-- /.aside-menu -->
          <!-- Skin changer -->
          <footer class="aside-footer border-top p-2">
              <button class="btn btn-light btn-block text-primary" data-toggle="skin"><span
                      class="d-compact-menu-none">Night mode</span> <i class="fas fa-moon ml-1"></i></button>
          </footer><!-- /Skin changer -->
      </div><!-- /.aside-content -->
  </aside><!-- /.app-aside -->

  <!-- .app-main -->
  <main class="app-main">
