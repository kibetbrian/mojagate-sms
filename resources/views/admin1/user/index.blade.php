@extends('admin1.layouts.app')
@section('Title', 'Index Page')
@section('content')
    <div class="wrapper">
        <!-- .page -->
        <div class="page">
            <!-- .page-inner -->
            <div class="page-inner">
                <!-- .page-title-bar -->
                <header class="page-title-bar">
                    <!-- .breadcrumb -->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">
                                <a href="#"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Tables</a>
                            </li>
                        </ol>
                    </nav><!-- /.breadcrumb -->
                    <!-- title -->
                    <h1 class="page-title"> User </h1>
                    <p class="text-muted"> </p><!-- /title -->
                </header><!-- /.page-title-bar -->
                <!-- .page-section -->
                <div class="page-section">
                    <!-- .card -->
                    <div class="card card-fluid">
                        <!-- .card-body -->
                        <div class="card-body">
                            <!-- .table -->
                            <table id="dt-responsive" class="table dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Firs Name</th>
                                        <th>Last Name</th>
                                        <th>Gender</th>
                                        <th>Date of Birth</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row"> <a href="#" class="question_content">D-R30221</a></th>
                                        <td>Wilson</td>
                                        <td>Kosgei</td>
                                        <td>Male</td>
                                        <td>
                                            <span class="label label-danger">23-02-1996</span>
                                        </td>
                                        <td>
                                            <a href="{{route('user-show')}}">
                                                <button class="btn btn-primary"> Show</button>
                                            </a>
                                            <a href="{{route('user-edit')}}">
                                                <button class="btn btn-primary"> Edit</button>
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table><!-- /.table -->
                        </div><!-- /.card-body -->
                    </div><!-- /.card -->
                </div><!-- /.page-section -->
            </div><!-- /.page-inner -->
        </div><!-- /.page -->
    </div>
@endsection
