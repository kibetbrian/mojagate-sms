@extends('admin.layouts.app')
@section('Title', 'Index Page')
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container">
                <h1>
                    Edit Driver
                </h1>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        @if ($message = Session::get('danger'))
                            <div class="alert alert-danger alert-block mt-2">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif

                        <form action="{{ route('driver-update', $driver->driver_code) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-xl-6 col-12">
                                    <h3>Personal Information</h3>
                                    <br>
                                    <div class="form-group">
                                        <h5>Drivers Name <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" class="form-control" required
                                                value="{{ $driver->name }}">
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <h5>ID number <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="number" name="id_number" class="form-control" required
                                                value="{{ $driver->id_number }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5>Mobile Number <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="phone" class="form-control" required
                                                value="{{ $driver->phone }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5>Age <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="number" name="age" class="form-control" required
                                                value="{{ $driver->age }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5> Address<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="address" value="{{ $driver->address }}"
                                                class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5>Town <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="town" name="town" value="{{ $driver->town }}"
                                                class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5>Upload Profile Picture <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="file" name="image" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-12">
                                    <h3>Employment Information</h3>
                                    <br>
                                    <div class="form-group">
                                        <h5>License Number <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="licence_no" value="{{ $driver->licence_no }}"
                                                class="form-control" required>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <h5>Cost of Hire <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="hiring_cost" value="{{ $driver->hiring_cost }}"
                                                class="form-control" required>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <h5>Contract Number <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="contract_no" value="{{ $driver->contract_no }}"
                                                class="form-control" required>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <h5>Availability <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select name="availability" id="select" required class="form-control">
                                                <option value="">Availabilty</option>
                                                <option value="active">Active</option>
                                                <option value="inactive">Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5>Type of Employment <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select class="form-control" name="type">
                                                <option>Select type of employment</option>
                                                <option value="Company employment">Company Employment</option>
                                                <option value="Contract employment">Contract</option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5>Basic Select <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select class="form-control" name="risky_level">
                                                <option>Select risky level</option>
                                                <option value="high">High</option>
                                                <option value="medium">Medium</option>
                                                <option value="low">Low</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <h5>Class type <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select class="form-control" name="class_type">
                                                <option>Select class type</option>
                                                <option value="B-Light vehicles">B-Light vehicles </option>
                                                <option value="C-Light Trucks">C-Light Trucks </option>
                                                <option value="B&C Combined">B&C Combined</option>
                                                <option value="CE -Heavy Commercial Truck">CE -Heavy Commercial Truck
                                                </option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="text-xs-right bt-1 pt-10">
                                        <button type="submit" class="btn btn-info">Edit </button>
                                    </div>

                                </div>
                            </div>


                        </form>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border bg-light">
                            <h4 class="box-title">Drivers Particulars</h4>
                        </div>
                        @if ($message = Session::get('message'))
                            <div class="alert alert-success alert-block mt-2">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        @if ($message = Session::get('dng'))
                            <div class="alert alert-danger alert-block mt-2">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <!-- /.box-header -->
                        <div class="box-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-fill" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home11"
                                        role="tab"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span
                                            class="hidden-xs-down">Licence details</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile11"
                                        role="tab"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span
                                            class="hidden-xs-down">Contract details</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#nots111" role="tab"><span
                                            class="hidden-sm-up"><i class="ion-email"></i></span> <span
                                            class="hidden-xs-down"> Work experience</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#messages11"
                                        role="tab"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span
                                            class="hidden-xs-down">Last incidence</span></a> </li>
                                @if ($driver->class_type == 'CE -Heavy Commercial Truck')
                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#info11"
                                            role="tab"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span
                                                class="hidden-xs-down">Short landings</span></a> </li>
                                @endif
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#nots11" role="tab"><span
                                            class="hidden-sm-up"><i class="ion-email"></i></span> <span
                                            class="hidden-xs-down"> Fuel Top upload</span></a> </li>

                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#feedback"
                                        role="tab"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span
                                            class="hidden-xs-down"> FeedBack</span></a> </li>

                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content tabcontent-border">
                                <div class="tab-pane active" id="home11" role="tabpanel">
                                    <div class="pad">
                                        @if ($licence)
                                            <div class="row">
                                                <div class="col-md-9 col-12">

                                                    <div class="profile-user-info">
                                                        <h3 class="mb-5">licence Number :<span
                                                                class="text-gray pl-10">{{ $licence->licence_number }}</span>
                                                        </h3>
                                                        <h3 class="mb-5">Issue Date :<span
                                                                class="text-gray pl-10">{{ $licence->issue_date }}</span>
                                                        </h3>

                                                        <h3 class="mb-5"> Expiry Date :<span
                                                                class="text-gray pl-10">{{ $licence->expiry_date }}</span>
                                                        </h3>

                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                        data-target="#modal-center">
                                                        Update Details
                                                    </button>
                                                    </td>
                                                </div>

                                            </div>

                                        @else
                                            <div class="row">
                                                <div class="col-md-9 col-12">
                                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                        data-target="#modal-center">
                                                        Add Licence Details
                                                    </button>
                                                </div>
                                                <div class="col-md-3 col-12">

                                                </div>

                                            </div>



                                        @endif

                                    </div>
                                </div>
                                <div class="tab-pane pad" id="profile11" role="tabpanel">
                                    <div class="pad">
                                        @if ($contractDetails)
                                            <div class="row">
                                                <div class="col-md-9 col-12">

                                                    <div class="profile-user-info">
                                                        <h3 class="mb-5">Contract details :<span
                                                                class="text-gray pl-10">{{ $contractDetails->contract_number }}</span>
                                                        </h3>
                                                        <h3 class="mb-5">Start Date :<span
                                                                class="text-gray pl-10">{{ $contractDetails->start_date }}</span>
                                                        </h3>

                                                        <h3 class="mb-5"> End Date :<span
                                                                class="text-gray pl-10">{{ $contractDetails->end_date }}</span>
                                                        </h3>

                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-12">
                                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                        data-target="#modal-center1">
                                                        Update Details
                                                    </button>
                                                    </td>
                                                </div>
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="col-md-9 col-12">
                                                    <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                        data-target="#modal-center1">
                                                        Add Contract Details
                                                    </button>
                                                </div>
                                                <div class="col-md-3 col-12">

                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="tab-pane pad" id="messages11" role="tabpanel">
                                    <div class="pad">
                                        @if ($lastincidences)
                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                data-target="#modal-center2">
                                                Add Last Incidences
                                            </button>
                                            <br>
                                            <div class="row">

                                                <div class="col-md-12 col-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Incidence</th>
                                                                    <th>Location</th>
                                                                    <th>Date</th>
                                                                    <th>Action</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($lastincidences as $item)

                                                                    <tr>
                                                                        <th scope="row"> <a href="#"
                                                                                class="question_content">{{ $item->incidence }}</a>
                                                                        </th>

                                                                        <td>{{ $item->location }}</td>
                                                                        <td>{{ $item->date }}</td>
                                                                        <td>
                                                                            <a
                                                                                href="{{ route('last-delete', ['id' => $item->id]) }}">
                                                                                <button class="btn btn-danger">
                                                                                    Delete</button>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="col-md-9 col-12">

                                                </div>
                                                <div class="col-md-3 col-12">

                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="tab-pane pad" id="info11" role="tabpanel">
                                    <div class="pad">
                                        @if ($shortlandings)
                                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                data-target="#modal-center3">
                                                Add Short Landings
                                            </button>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12 col-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Product</th>
                                                                    <th>Unit</th>
                                                                    <th>Original Quantity</th>
                                                                    <th>Short Landings</th>
                                                                    <th>Location</th>
                                                                    <th>Action</th>


                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($shortlandings as $item)

                                                                    <tr>
                                                                        <th scope="row"> <a href="#"
                                                                                class="question_content">{{ $item->product }}</a>
                                                                        </th>
                                                                        <td>{{ $item->unit }}</td>
                                                                        <td>{{ $item->original_quantity }}</td>
                                                                        <td>{{ $item->short_landing }}</td>
                                                                        <td>{{ $item->location }}</td>
                                                                        <td>
                                                                            <a
                                                                                href="{{ route('short-delete', ['id' => $item->id]) }}">
                                                                                <button class="btn btn-danger">
                                                                                    Delete</button>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach

                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="tab-pane pad" id="nots11" role="tabpanel">
                                    <div class="pad">
                                        @if ($fueltopUps)
                                            <button type="button" class="btn btn-primary btn-sm pull right"
                                                data-toggle="modal" data-target="#modal-center4">
                                                Add Fuel Top Ups
                                            </button>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12 col-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Trip Details</th>
                                                                    <th>Quantity</th>
                                                                    <th>Date</th>
                                                                    <th>Action</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($fueltopUps as $item)
                                                                    <tr>
                                                                        <th scope="row"> <a href="#"
                                                                                class="question_content">{{ $item->trip_details }}</a>
                                                                        </th>
                                                                        <td>{{ $item->quantity }}</td>
                                                                        <td>{{ $item->date }}</td>
                                                                        <td>
                                                                            <a
                                                                                href="{{ route('fuel-delete', ['id' => $item->id]) }}">
                                                                                <button class="btn btn-danger">
                                                                                    Delete</button>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="col-md-9 col-12">

                                                </div>
                                                <div class="col-md-3 col-12">

                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="tab-pane pad" id="feedback" role="tabpanel">
                                    <div class="pad">
                                        @if ($feedbacks)
                                            <button type="button" class="btn btn-primary btn-sm pull right"
                                                data-toggle="modal" data-target="#modal-center8">
                                                Add Comments and Feedback
                                            </button>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12 col-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Feedback</th>
                                                                    <th>Rating</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($feedbacks as $item)
                                                                    <tr>
                                                                        <th scope="row"> <a href="#"
                                                                                class="question_content">{{ $item->feedback }}</a>
                                                                        </th>
                                                                        <td>{{ $item->rating }}</td>
                                                                        <td>
                                                                            <a
                                                                                href="{{ route('feedback-delete', ['id' => $item->id]) }}">
                                                                                <button class="btn btn-danger">
                                                                                    Delete</button>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="col-md-9 col-12">

                                                </div>
                                                <div class="col-md-3 col-12">

                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                                <div class="tab-pane pad" id="nots111" role="tabpanel">
                                    <div class="pad">
                                        @if ($workExperience)
                                            <button type="button" class="btn btn-primary btn-sm pull right"
                                                data-toggle="modal" data-target="#modal-center5">
                                                Add work experience
                                            </button>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12 col-12">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Company</th>
                                                                    <th>From- To</th>
                                                                    <th>Reason for Leaving</th>
                                                                    <th>Action</th>

                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($workExperience as $item)
                                                                    <tr>
                                                                        <th scope="row"> <a href="#"
                                                                                class="question_content">{{ $item->company }}</a>
                                                                        </th>
                                                                        <td>{{ $item->from }} - {{ $item->to }}
                                                                        </td>
                                                                        <td>{{ $item->reason }}</td>
                                                                        <td>
                                                                            <a
                                                                                href="{{ route('work-experince-delete', ['id' => $item->id]) }}">
                                                                                <button class="btn btn-danger">
                                                                                    Delete</button>
                                                                            </a>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                </div>
                                            </div>
                                        @else
                                            <div class="row">
                                                <div class="col-md-9 col-12">

                                                </div>
                                                <div class="col-md-3 col-12">

                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection

<!-- Modal for licensce -->
<div class="modal center-modal fade" id="modal-center" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Licence Details</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('license') }}" method="POST">
                    @csrf


                    <div class="form-group">
                        <h5>Licence Number <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="licence_number" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>

                    </div>
                    <div class="form-group">
                        <h5>Issue Date <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="date" name="issue_date" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>Expiry Date <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="date" name="expiry_date" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>

            </div>
            <div class="modal-footer modal-footer-uniform">
                <input type="text" hidden name="driver_code" value="{{ $driver->driver_code }}" class="form-control"
                    data-validation-required-message="This field is required">
                <button type="button" class="btn btn-bold btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-bold btn-primary float-right">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /.modal -->



<!-- Modal  for conntract details-->
<div class="modal center-modal fade" id="modal-center1" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Contract Details</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('contract-details') }}" method="POST">
                    @csrf


                    <div class="form-group">
                        <h5>Contract Number <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="contract_number" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>

                    </div>
                    <div class="form-group">
                        <h5>Start Date <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="date" name="start_date" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>End Date <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="date" name="end_date" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>

            </div>
            <div class="modal-footer modal-footer-uniform">
                <input type="text" hidden name="driver_code" value="{{ $driver->driver_code }}" class="form-control"
                    data-validation-required-message="This field is required">
                <button type="button" class="btn btn-bold btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-bold btn-primary float-right">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /.modal -->



<!-- Modal last incidences -->
<div class="modal center-modal fade" id="modal-center2" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Last Incidences</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('last-incidence') }}" method="POST">
                    @csrf


                    <div class="form-group">
                        <h5>Incidence <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="incidence" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>

                    </div>
                    <div class="form-group">
                        <h5> Date <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="date" name="date" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>Location<span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="location" class="form-control" required
                                data-validation-required-message="This field is required">
                            <small>Place where the incidence occurred</small>
                        </div>
                    </div>

            </div>
            <div class="modal-footer modal-footer-uniform">
                <input type="text" hidden name="driver_code" value="{{ $driver->driver_code }}" class="form-control"
                    data-validation-required-message="This field is required">
                <button type="button" class="btn btn-bold btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-bold btn-primary float-right">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /.modal -->

<!-- Modal for shortlandings -->
<div class="modal center-modal fade" id="modal-center3" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add short landings</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('short-landings') }}" method="POST">
                    @csrf


                    <div class="form-group">
                        <h5>Product <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="product" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>

                    </div>
                    <div class="form-group">
                        <h5>Unit <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="unit" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>Original Quantity <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="original_quantity" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>

                    <div class="form-group">
                        <h5>Short Landing <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="short_landings" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>Location <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="location" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>

            </div>
            <div class="modal-footer modal-footer-uniform">
                <input type="text" hidden name="driver_code" value="{{ $driver->driver_code }}" class="form-control"
                    data-validation-required-message="This field is required">
                <button type="button" class="btn btn-bold btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-bold btn-primary float-right">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /.modal -->


<!-- Modal  Fuel top ups-->
<div class="modal center-modal fade" id="modal-center4" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Fuel Top Ups</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('fuel-top-ups') }}" method="POST">
                    @csrf


                    <div class="form-group">
                        <h5>Quantity <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="quantity" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>

                    </div>
                    <div class="form-group">
                        <h5> Date <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="date" name="date" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>Trip Details <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="trip_details" class="form-control"
                                placeholder="Mariakani- Athi-River" required
                                data-validation-required-message="This field is required">
                            <small>Specify trip From-To</small>
                        </div>
                    </div>

            </div>
            <div class="modal-footer modal-footer-uniform">
                <input type="text" hidden name="driver_code" value="{{ $driver->driver_code }}" class="form-control"
                    data-validation-required-message="This field is required">
                <button type="button" class="btn btn-bold btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-bold btn-primary float-right">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /.modal -->


<!-- Modal  Fuel top ups-->
<div class="modal center-modal fade" id="modal-center8" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">FeedBack</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('feedback') }}" method="POST">
                    @csrf


                    <div class="form-group">
                        <h5>FeedBack <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <textarea name="feedback" id="" cols="50" rows="5">

                            </textarea>

                        </div>

                    </div>
                    <div class="form-group">
                        <h5> Rating <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="number" name="rating" class="form-control" required
                                data-validation-required-message="This field is required">
                            <small>Rate Driver </small>
                            <small>1 (lowest) 5 (Highest)</small>
                        </div>
                    </div>
            </div>
            <div class="modal-footer modal-footer-uniform">
                <input type="text" hidden name="driver_code" value="{{ $driver->driver_code }}" class="form-control"
                    data-validation-required-message="This field is required">
                <button type="button" class="btn btn-bold btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-bold btn-primary float-right">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /.modal -->

<!-- Modal  Fuel top ups-->
<div class="modal center-modal fade" id="modal-center5" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Work Experience</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ route('work-experince') }}" method="POST">
                    @csrf


                    <div class="form-group">
                        <h5>Company <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="company" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>

                    </div>
                    <div class="form-group">
                        <h5> From Date <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="date" name="from" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>To Date <span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="date" name="to" class="form-control" required
                                data-validation-required-message="This field is required">
                        </div>
                    </div>
                    <div class="form-group">
                        <h5>Reason for leaving<span class="text-danger">*</span></h5>
                        <div class="controls">
                            <input type="text" name="reason" class="form-control">
                        </div>
                    </div>


            </div>
            <div class="modal-footer modal-footer-uniform">
                <input type="text" hidden name="driver_code" value="{{ $driver->driver_code }}" class="form-control"
                    data-validation-required-message="This field is required">
                <button type="button" class="btn btn-bold btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-bold btn-primary float-right">Save changes</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- /.modal -->
