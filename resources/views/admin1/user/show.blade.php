@extends('admin1.layouts.app')
@section('Title', 'Index Page')
@section('content')
    <!-- .wrapper -->
    <div class="wrapper">
        <!-- .page -->
        <div class="page has-sidebar has-sidebar-fluid has-sidebar-expand-xl">
            <!-- .page-inner -->
            <div class="page-inner page-inner-fill position-relative">
                <header class="page-navs bg-light shadow-sm">
                    <!-- .input-group -->
                    <div class="input-group has-clearable">
                        <button type="button" class="close" aria-label="Close"><span aria-hidden="true"><i
                                    class="fa fa-times-circle"></i></span></button> <label class="input-group-prepend"
                            for="searchClients"><span class="input-group-text"><span
                                    class="oi oi-magnifying-glass"></span></span></label> <input type="text"
                            class="form-control" id="searchClients" data-filter=".board .list-group-item"
                            placeholder="Find clients">
                    </div><!-- /.input-group -->
                </header><button type="button" class="btn btn-primary btn-floated position-absolute" data-toggle="modal"
                    data-target="#clientNewModal" title="Add new client"><i class="fa fa-plus"></i></button> <!-- board -->
                <div class="board p-0 perfect-scrollbar">
                    <!-- .list-group -->
                    <div class="list-group list-group-flush list-group-divider border-top" data-toggle="radiolist">
                        <!-- .list-group-item -->

                        <div class="list-group-item" data-toggle="sidebar" data-sidebar="show">
                            <a href="#" class="stretched-link"></a> <!-- .list-group-item-figure -->
                            <div class="list-group-item-figure">
                                <?php $firstStringCharacter = substr($res->firstName, 0, 1); ?>
                                <div class="tile tile-circle bg-indigo">{{$firstStringCharacter}}</div>

                            </div><!-- /.list-group-item-figure -->
                            <!-- .list-group-item-body -->
                            <div class="list-group-item-body">
                                <p class="list-group-item-title">{{$res->firstName }} {{$res->lastName}} </p>
                                <p class="list-group-item-text">{{$res->dob }} </p>
                            </div><!-- /.list-group-item-body -->
                        </div><!-- /.list-group-item -->

                    </div><!-- /.list-group -->
                </div><!-- /board -->
            </div><!-- /.page-inner -->
            <!-- .page-sidebar -->
            <div class="page-sidebar bg-light">
                <!-- .sidebar-header -->
                <header class="sidebar-header d-xl-none">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">
                                <a href="#" onclick="Looper.toggleSidebar()"><i
                                        class="breadcrumb-icon fa fa-angle-left mr-2"></i>Back</a>
                            </li>
                        </ol>
                    </nav>
                </header><!-- /.sidebar-header -->

                <div class="sidebar-section sidebar-section-fill">
                    <h1 class="page-title">
                        <i class="far fa-building text-muted mr-2"></i> {{$res->firstName }} {{$res->lastName}}
                    </h1>
                    <p class="text-muted">{{$res->dob }}</p><!-- .nav-scroller -->
                    <div class="nav-scroller border-bottom">
                        <!-- .nav-tabs -->
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active show" data-toggle="tab" href="#client-billing-contact">Personal
                                    Data &
                                    Contact</a>
                            </li>

                        </ul><!-- /.nav-tabs -->
                    </div><!-- /.nav-scroller -->
                    <!-- .tab-content -->

                    <div class="tab-content pt-4" id="clientDetailsTabs">
                        <br>
                        <div class="row">
                            <!-- grid column -->
                            <div class="col-lg-12">
                                <!-- .sidebar-section -->
                                @if ($message = Session::get('msg'))
                                    <div class="col-lg-4">
                                        <div class="alert alert-success alert-dismissible fade show">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{{ $message }}</strong>.
                                        </div>
                                    </div><!-- /grid column -->
                                @endif
                                @if ($message = Session::get('danger'))
                                    <div class="col-lg-4">
                                        <div class="alert alert-success alert-dismissible fade show">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{{ $message }}</strong>.
                                        </div>
                                    </div><!-- /grid column -->
                                @endif
                            </div>
                        </div>
                        <!-- .tab-pane -->
                        <div class="tab-pane fade show active" id="client-billing-contact" role="tabpanel"
                            aria-labelledby="client-billing-contact-tab">
                            <!-- .card -->
                            <div class="row">
                                <!-- grid column -->
                                <div class="col-lg-9">
                                    <!-- .card -->
                                    <div class="card">

                                        <div class="card-header d-flex">
                                            <button type="button" class="btn btn-primary ml-auto" data-toggle="modal"
                                                data-target="#work" title="Add new client"><i class="fa fa-plus"></i>Edit
                                                User</button>

                                        </div><!-- /.card-header -->
                                        <div class="card-body">
                                            <h2 class="card-text text-gray pl-10"> Personal Information </h2><!-- .card-body -->
                                            <h5 class="card-text">User Name : <span class="text-gray pl-10">{{$res->firstName }} {{$res->lastName}}</span>
                                            </h5>
                                            <h5 class="card-text"> Email <span
                                                    class="text-gray pl-10">{{$res->email }}</span>
                                            </h5>
                                            <h5 class="card-text"> Gender :<span class="text-gray pl-10">{{$res->gender }}</span>
                                            </h5>

                                            <h5 class="card-text">Date of Birth <span
                                                    class="text-gray pl-10">{{$res->dob }}</span>
                                            </h5>

                                        </div><!-- /.card-body -->
                                    </div><!-- /.card -->
                                </div><!-- /grid column -->
                            </div>
                        </div><!-- /.tab-pane -->
                        <!-- .tab-pane -->
                    </div><!-- /.tab-content -->
                </div><!-- /.sidebar-section -->
            </div><!-- /.page-sidebar -->
             <!-- .modal -->
             <form action="{{route('user-update',$res->id)}}" method="POST">
                @csrf
                <div class="modal fade" id="work" tabindex="-1" role="dialog" aria-labelledby="clientNewModalLabel"
                    aria-hidden="true">
                    <!-- .modal-dialog -->
                    <div class="modal-dialog" role="document">
                        <!-- .modal-content -->
                        <div class="modal-content">
                            <!-- .modal-header -->
                            <div class="modal-header">
                                <h6 id="clientNewModalLabel" class="modal-title inline-editable">
                                    Update user
                                </h6>
                            </div><!-- /.modal-header -->
                            <!-- .modal-body -->
                            <div class="modal-body">
                                <!-- .form-row -->
                                <div class="form-row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="cnContactName">First Name</label> <input type="text" name="firstName" value="{{$res->firstName}}"
                                                id="cnContactName" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="cnContactName">Last Name</label> <input type="text" name="lastName" value="{{$res->lastName}}"
                                                id="cnContactName" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="cnContactEmail">Email</label> <input type="email" value="{{$res->email}}"
                                                name="email" id="cnContactEmail" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="cnStreet">Gender</label>  <select name="gender" id="" class="form-control" id="formGroupExampleInput2">
                                                <option value="">Select Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="cnSuite">Date of Birth</label> <input type="date" id="cnSuite" name="dob"
                                                class="form-control">
                                        </div>

                                    </div>
                                </div><!-- /.form-row -->
                            </div><!-- /.modal-body -->
                            <!-- .modal-footer -->
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button> <button type="button"
                                    class="btn btn-light" data-dismiss="modal">Close</button>
                            </div><!-- /.modal-footer -->
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
            </form><!-- /.modal -->
            <!-- .modal -->
        </div><!-- /.page -->
    </div><!-- /.wrapper -->
@endsection
