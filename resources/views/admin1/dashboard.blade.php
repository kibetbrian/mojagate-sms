@extends('admin1.layouts.app')
@section('Title', 'Index Page')
@section('content')
    <div class="wrapper">
        <!-- .page -->
        <div class="page">
            <!-- .page-inner -->
            <div class="page-inner">
                <!-- .page-title-bar -->
                <header class="page-title-bar">
                    <!-- .breadcrumb -->
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active">
                                {{--  <a href="#"><i class="breadcrumb-icon fa fa-angle-left mr-2"></i>Tables</a>  --}}
                            </li>
                        </ol>
                    </nav><!-- /.breadcrumb -->
                    <!-- title -->
                    {{--  <h1 class="page-title"> User </h1>  --}}
                    <p class="text-muted"> </p><!-- /title -->
                </header><!-- /.page-title-bar -->
                <!-- .page-section -->

                <div class="row">
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-body">
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                              Send Message &nbsp;
                              <i class="fa fa-envelope"></i></button>

                            <!-- Modal -->
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                              <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Send Your Message Here</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <form method="post" action="{{ route('receive-message') }}">
                                        {{ csrf_field() }}
                                    <div class="modal-body">
                                            <div class="form-group">
                                            <label for="exampleFormControlInput1">Enter Phone Number</label>
                                            <input type="tel" class="form-control"  name="phone" id="phone" placeholder="phone number...">
                                            </div>

                                            <div class="form-group">
                                            <label for="exampleFormControlTextarea1">Enter Message</label>
                                            <textarea class="form-control" name="message" id="message" rows="3"></textarea>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit"  class="btn btn-primary">Send</button>
                                    </div>
                                </form>
                                </div>
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="card">
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                  <tr>
                                    <th scope="col">Index</th>
                                    <th scope="col">Phone Number</th>
                                    <th scope="col">Message</th>
                                    <th scope="col">&nbsp;&nbsp;&nbsp; Status</th>
                                    <th scope="col">Time Sent</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach ($records as $record)
                                    <tr>
                                        <th>{{ $record->id }}</th>
                                        <td>{{ $record->phone }}</td>
                                        <td>{{ $record->message }}</td>
                                        <td>
                                            @if($record->status =='0')
                                            <span class="badge badge-soft-warning ml-sm-3">
                                                <span class="legend-indicator bg-warning"></span>Pending
                                            </span>
                                            @elseif ($record->status =='1')
                                                <span class="badge badge-soft-success ml-sm-3">
                                                    <span class="legend-indicator bg-success"></span>Sent
                                                </span>

                                            @endif
                                        </td>
                                        <td>{{ $record->time_sent }}</td>
                                      </tr>
                                    @endforeach
                                </tbody>
                              </table>
                        </div>
                      </div>
                    </div>
                  </div>
                <!-- /.page-section -->
            </div><!-- /.page-inner -->
        </div><!-- /.page -->
    </div>

@endsection

